package Nested_static_classes_enum;

public class UsageHuman {
    public static void main(String[] args) {
        Human man = new Human("Mike", 20, Human.Relations.SINGLE);
        Human woman = new Human("Mary", 20, Human.Relations.SINGLE);
        System.out.println(man);
        System.out.println(woman);

        man.setRelations(Human.Relations.IN_LOVE);
        woman.setRelations(Human.Relations.MARRIED);
        System.out.println(man);
        System.out.println(woman);
    }
}